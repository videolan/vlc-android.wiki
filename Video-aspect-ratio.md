Here is an explanation of how the aspect ratios work in VLC for Android's video player.

| Aspect Ratio | Explanation | Image is larger | Screen is larger | Black bars | Distorted | Cropped |
| ------------ | ----------- | --------------- | ---------------- | ---------- |---------- | ------- |
|   Best fit   | The video will take the largest size possible without distortion and will add black bars if needed |  ![device_4_3-best_fit](uploads/087f2dfb46c1b4b46fbf1a96ed288c05/device_4_3-best_fit.png)              |       ![device_16_9-best_fit](uploads/945bcc6918ea8c37a0afcd1476dc04d6/device_16_9-best_fit.png)           | :white_check_mark: | :x: | :x: |
|   Fit screen | The video will cover the whole screen, without distortion but may be cropped|    ![device_4_3-fit-screen](uploads/670fa68fc10c162f13e435b0e3ad3f0b/device_4_3-fit-screen.png)             |        ![device_16_9-fit-screen](uploads/dde1f2b701654d07f3dac2b04d968d59/device_16_9-fit-screen.png)          | :x: | :x: | :white_check_mark: |
|   Fill       | The video will be displayed as a whole and cover the whole screen but may be distorted |      ![device_4_3-fill](uploads/cabada1acf8584a3285c5c5db9037a06/device_4_3-fill.png)           |        ![device_16_9-fill](uploads/a92416ee14619a4a517f2fb810a649cc/device_16_9-fill.png)          | :x: | :white_check_mark: | :x: |
|   Fixed (16:9, 4:3, ...)       | Useful when the video has been encoded in a bad ratio. It will render the video in the fixed ratio and then use Best fit. | |               | :white_check_mark: | :white_check_mark: | :x: |

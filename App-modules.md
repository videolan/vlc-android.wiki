```mermaid
graph TD
    A[app]
    B[vlc-android]
    C[medialibrary]
    D[libvlcjni]
    E[television]
    F[resources]
    G[tools]
    H[moviepedia]
    I[live-plot-graph]
    J[mediadb]

    A --> B
    A --> E
    E --> B
    E --> H
    I --> G
    J --> F
    J --> G
    H --> B
    H --> G
    F --> C
    F --> D
    F --> G
    B --> D
    B -.->|debug:aar| D
    B --> C
    B -.->|debug:aar| C
    B --> G
    B --> F
    B --> J
    B --> I
    C --> D
    C -.->|debug:aar| D




```
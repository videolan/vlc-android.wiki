To allow us to understand your issue more easily, please use a template corresponding to your situation when you open your ticket.

Here is how to do it.

In the [Issue screen](https://code.videolan.org/videolan/vlc-android/-/issues), click on the `New issue` button. 

You will land to a page looking like this:

![image](uploads/b06f4f0ef1148cf61e63d3add28183fa/image.png)

In the description part, please choose a template depending on what you want to report.

- If you want to report a bug in VLC for Android, please use the `VLC for Android - Feature Request` template.
- If you want to ask for a new feature, please use the `VLC for Android - Feature Request` template.
- If you're a libvlc java developer, please use the `libvlc - Bug` template.
- If you just want to ask questions on how to use VLC for Android, please use [our forum](https://forum.videolan.org/viewforum.php?f=35)

**Please note that any ticket not using a template may be closed without notice as it won't provide the necessary information.**

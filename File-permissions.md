Here is the way the permission work depending on the user's device Android version

```mermaid
graph LR
    Z[User's device] --> A
    Z --> C
    Z --> F
    A[Android 23 or less] -->|New install| B(No permission asked<br />Manifest is sufficient)
    C[Android 23  to 29] -->|New install| D(Ask Read in onboarding)
    C -->|Upgrade from before 23 | E(Empty view asks for Read)
    F[Android 30+] --> |New install| G(Ask All Access in onboarding)
    F -->|Upgrade with no permission| E
    F -->|Upgrade with read permission| H(Snackbar in main to ask for All Access)
    I(Indexing possible)
    J(Indexing impossible)
    K(Partial indexing: only Android media)
    D -->|Granted| I
    D -->|Denied| J
    E -->|Granted| I
    G -->|Granted| I
    G -->|Denied| J
    H -->|Granted| I
    H -->|Denied| K
    style I fill:#6f6,stroke:#6f6
    style J fill:#faa,stroke:#faa
    style K fill:#f80,stroke:#f80
    linkStyle 9,11,12,14 stroke:#6f6,stroke-width:4px,stroke-dasharray: 5 5
    linkStyle 10,13,15 stroke:#faa,stroke-width:4px,stroke-dasharray: 12 2;
```
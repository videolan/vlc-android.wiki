The project is now ready to build VLC 3 and VLC 4.

### Build VLC 4 from Android studio

To build vlc-android with VLC 4, gradle needs a property: `-PforceVlc4=true`.

It can be directly added to the gradle task command line or in Android Studio globally.

![Screenshot_20241128_140709](uploads/5b3f7c0728fba2ff0f159d55b9174f36/Screenshot_20241128_140709.png)

Another way to do that is to force the `vlcMajorVersion` variable in the root project `build.gradle` file to `4`.

### Build VLC 4 from `compile.sh`

A new parameter has been added to the script. Juste use `-vlc4` to build.

For example: `./buildsystem/compile.sh -vlc4 run`

:warning: the script is not able to automatically clean your last build. If you already built vlc 3, you'll have to remove these folders:

- ./libvlcjni
- ./vlc
- ./medialibrary/medialibrary

You will also have to launch `gradle clean`.

Note that the NDK requirements are different on VLC 3 and VLC 4.

- VLC 3: you'll need NDK 21
- VLC 4: You'll need NDK 25

You can download both here: https://developer.android.com/ndk/downloads

You'll also have to set a different environment variable depending on the version you want to build:

- VLC 3: `export ANDROID_NDK=/path/to/android-ndk-r21`
- VLC 4: `export ANDROID_NDK=/path/to/android-ndk-r25`

### CI

The jobs have been duplicated to build both of these versions. During the transition phase, both jobs will be launched.

As VLC 4 needs NDK 25 while VLC 3 needs NDK 21, a different docker image is used for the jobs.

- VLC 3: https://code.videolan.org/videolan/docker-images/-/tree/master/vlc-debian-android-3.0
- VLC 4: https://code.videolan.org/videolan/docker-images/-/tree/master/vlc-debian-android

### Technical implementation

Conditional compilation is not a thing in Java/kt. To make it work, we have to add an abstraction layer and split it in different sourceSet.

This is done using this line in the `build.gradle` files of `:application:vlc-android` and `:medialibrary`

```
java.srcDirs = rootProject.ext.vlcMajorVersion == 4 ? ['src', 'vlc4/src'] : ['src', 'vlc3/src']
```

The code in these directories is then applied at compile time depending on the VLC version we want to build.

Keep in mind that any change in one of these two directories has to be replicated in the other one and that the signatures have to be the same. If you fail to do that, it should trigger a pipeline fail in your Merge request.

### Nightlies

For now, only vlc3 nightlies are generated. But everything is ready to do VLC 4 nightlies as well (only a new schedule is needed)

### In-Depth presentation

[Migration_to_VLC_4.pdf](uploads/b0efefd0d8b1b4fbb6f7f402c02e1efc/Migration_to_VLC_4.pdf)
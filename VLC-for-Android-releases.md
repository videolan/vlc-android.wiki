### Availability

VLC for Android is released in different places:

| Name   | Variant | Link  | Signed by  |
| ------ | ------ | ------ | ------ |
| VideoLAN's website | Release, Beta, Nightly | Releases: https://get.videolan.org/vlc-android/<br/>Nightlies: https://nightlies.videolan.org/       | VideoLAN |
| Google Play       | Release, Beta|   https://play.google.com/store/apps/details?id=org.videolan.vlc     | VideoLAN for devices running Android 10 or less, Google otherwise |
| Amazon Appstore| Release|https://www.amazon.com/VLC-Mobile-Team-for-Fire/dp/B00U65KQMQ|VideoLAN|
|Huawei AppGallery| Release|https://appgallery.huawei.com/#/app/C101924579|VideoLAN|
|F-Droid| Release|https://f-droid.org/en/packages/org.videolan.vlc/|F-Droid|
#### Install transifex cli tool

In the project root directory, use:

`curl -o- https://raw.githubusercontent.com/transifex/cli/master/install.sh | bash`

#### Push

It's automatic

#### Pull

`TX_TOKEN=[YOUR_TOKEN] ./tx pull --use-git-timestamps` 

You can get your token at https://app.transifex.com/user/settings/api/
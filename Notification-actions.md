# Android <=12

## Slots

1. Mobile

Extended ![image](uploads/d70639c6e2760eb0bfc4e6fb9d0336df/image.png) Compact ![image](uploads/818446589ed5de6a9fa25238a8348a17/image.png)

2. Auto

![image](uploads/7b2a22e6a5f92b326b9171cd7ba41402/image.png)

### Mobile "one media play queue"
| Slot | Audio | Podcast | Compact view Audio | Compact view Podcast |
|------|-------|---------|--------------------|----------------------|
| 1 | previous | speed | previous | rewind 10 |
| 2 | next | bookmark | next | forward 10 |
| 3 | rewind 10 | rewind 10 |  |  |
| 4 | forward 10 | forward 10 |  |  |

### Mobile "multiple medias play queue (same as above)"
| Slot | Audio | Podcast | Compact view Audio | Compact view Podcast |
|------|-------|---------|--------------------|----------------------|
| 1 | previous | speed | previous | previous |
| 2 | next | bookmark | next | next |
| 3 | rewind 10 | rewind 10 |  |  |
| 4 | forward 10 | forward 10 |  |  |

### Auto "one media play queue"
| Slot | Audio | Podcast |
|------|-------|---------|
| 1 | previous | rewind 10 |
| 2 | next (or nothing) | forward 10 |
| 3 | shuffle | speed |
| 4 | repeat | bookmark |

NB: 1 and 3 are kept and other added to overflow if setting enabled, only for audio. Podcasts are not affected by the settings

### Auto "multiple medias play queue"
| Slot | Audio | Podcast |
|------|-------|---------|
| 1 | previous | previous |
| 2 | next (or nothing) | next (or nothing) |
| 3 | shuffle | shuffle |
| 4 | repeat | repeat |

NB: 1, 2 and 3 are kept and others added to overflow if setting enabled, only for audio. Podcasts are affected this time.

# Android 13+

## Slots

1. Mobile

Extended ![image](uploads/97b2674404c78d41954efa83a93a4d3f/image.png)

Compact ![image](uploads/6954850a4ee2c3f43d5847fcc0adb44c/image.png)

### Mobile "one media play queue"
| Slot | Audio | Podcast | Compact view Audio | Compact view Podcast |
|------|-------|---------|--------------------|----------------------|
| 1 | previous | rewind 10 | previous | rewind 10 |
| 2 | next | forward 10 | next | forward 10 |
| 3 | shuffle (disabled) :warning:  | speed :warning: |  |  |
| 4 | repeat (disabled) :warning:  | bookmark :warning: |  |  |

### Mobile "multiple medias play queue (same as above)"
| Slot | Audio | Podcast | Compact view Audio | Compact view Podcast |
|------|-------|---------|--------------------|----------------------|
| 1 | previous | speed | previous | previous |
| 2 | next | bookmark | next | next |
| 3 | shuffle (disabled) :warning: | shuffle (disabled) :warning: |  |  |
| 4 | repeat (disabled) :warning: | repeat (disabled) :warning: |  |  |

### Auto "one media play queue"
| Slot | Audio | Podcast |
|------|-------|---------|
| 1 | previous | rewind 10 |
| 2 | next (or nothing) | forward 10 |
| 3 | shuffle | speed |
| 4 | repeat | bookmark |

NB: 1 and 3 are kept and other added to overflow if setting enabled, only for audio. Podcasts are not affected by the settings

### Auto "multiple medias play queue"
| Slot | Audio | Podcast |
|------|-------|---------|
| 1 | previous | previous |
| 2 | next (or nothing) | next (or nothing) |
| 3 | shuffle | shuffle |
| 4 | repeat | repeat |

NB: 1, 2 and 3 are kept and others added to overflow if setting enabled, only for audio. Podcasts are affected this time.
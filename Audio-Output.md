## Audio output

This is how the audio output to be used is determined. Dotted lines are a possible choice when full lines are the default choice.

```mermaid
graph TD
    A[Audio output] 
    B[Amazon hack?]
    
    X[Aaudio]
    Y[Audiotrack]
    Z[OpenSLES]
    W[VLC version]
    U[User choice]
    V[User choice]

    M[No]
    N[Yes]
    O[Yes but fallback<br/>on Audiotrack automatically]

    A --> B
    B -->|yes| Z
    B -->|no| W

    W -->|VLC 3| U
    W -->|VLC4| V
    U --> Y
    U -.-> Z
    V --> X
    V -.-> Y
    V -.-> Z

    X -->|Show passthrough| O
    Y -->|Show passthrough| N
    Z -->|Show passthrough| M
```

NB:
- Amazon devices are not compatible with AudioTrack. So it fallbacks automatically to OpenSLES in libvlc if an Amazon device is detected
- AAudio cannot do passthrough. If the user chooses AAudio and checks passthrough, AudioTrack will be used instead (done in libvlc)
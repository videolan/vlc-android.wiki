[Where to get VLC for Android](VLC-for-Android-releases) 

[App README](https://code.videolan.org/videolan/vlc-android/-/blob/master/README.md)

[Video aspect ratio](Video-aspect-ratio) 

[App modules](App-modules)

[Fastlane](https://code.videolan.org/videolan/vlc-android/-/blob/master/buildsystem/automation/README.md)

[Publish libs on Maven](https://code.videolan.org/videolan/vlc-android/-/blob/master/buildsystem/maven/README.md)

[Permissions](File-permissions)

[Notification actions](Notification-actions)

[Build VLC 4](VLC-4)

[Audio output decision chart](Audio-Output)

[Use transifex](Transifex-cli)

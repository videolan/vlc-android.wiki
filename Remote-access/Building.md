The website files are in the remote access module assets, under the `dist` directory. There are multiple ways to (re)generate them.

### With the `compile.sh` script

Use the `-web` option (not yet implemented)

### Release / Regenerate once for all

Navigate to `buildsystem/network-sharing-server`. Use `npm run build`. The correct files will be generated and copied in the remote access's assets

### Development

To ease the development, you do not have to build for every change. You can serve the website app directly using vue-cli and will see real time changes without rebuilding the Android app.

To do so, navigate to `buildsystem/network-sharing-server`. Use `npm run serve`.

See also: [Remote access architecture](Remote-access/Architecture)

### XHR

All content queries are achieved using XHR calls. 

- Server side: ktor routes
- Client side: axios

### Websockets

Some features need real time events. To do that, the website uses websockets. 

Non exhaustive list:

1. From app to website
- Volume
- Now playing (current media, current player state, ...)
- Play queue (list of media)

2. From website to app
- Playback controls (play, pause, next, repeat, ...)
- set progress
- get/set volume
- Play queue controls (remove, move media in/from play queue)

### Icons

Some icons are specific to VLC Android. They are served by ktor using the `/icon?id=[app_icon_id]&width=[desiredWidth]` route. Other (pure material) icons are directly served as svg files from the vue app.

### Long polling fallback

When the websockets are not available (in the browser or not implemented by the server), the client falls back to long polling to manage the playback status / control.

```mermaid
sequenceDiagram
    participant Web user
    participant Client
    participant Server
    participant Player
    participant App user
   Client->>Server: Ask for websocket indentification
   Note over Server: Server doen't support websockets
   Server->>Client: HTTP 500/404
   Note over Client, Server: Start long polling instead
   Client->>Server: XHR `longpolling` API
   loop Awaits for event
        Note over Server: Nothing happened and the timeout is reached

    end
    Server->>Client: HTTP 408 Request Timeout
 
    Note over Client, Server: long polling exhausted, starting again
    Client->>Server: XHR `longpolling` API
    loop Awaits for event
        Player->>Server: Something happened<br/>(progress changed, media changed, ...)
        Server->>Client: HTTP 200 with body decribing what heppened

    end

    Note over Client, Server: long polling managed, starting again
    Client->>Server: XHR `longpolling` API
    loop Awaits for event
        App user->>Player: Action done<br/>(add  a media to playqueue, pause, ...) 
        Player->>Server: Something happened
        Server->>Client: HTTP 200 with body decribing what heppened

    end

    Note over Client, Server: long polling managed, starting again
    Client->>Server: XHR `longpolling` API
    loop Awaits for event
        Note over Server: Waiting

    end

    Web user->>Client: Action done<br/>(add  a media to playqueue, pause, ...)
    Client->>Server: XHR `playbackevent` API with body
    Note over Server: longpolling above trigerred<br/>to update the client
```
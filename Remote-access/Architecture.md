### Remote access architecture

Here is a graph describing the release and development architecture of the remote access

```mermaid
flowchart TD
   
    B[Remote access code]
    B --npm run build--> C
    B --npm run serve-->G

    subgraph Release
        A[VLC-Android]
        C[Compiled] 
        D[Extract asset in\napp's private folder]
        F[Async endpoints]
        
        C -- Put in assets --> A
        A --Runtime-->D
       
    end
    subgraph Dev
        G[Local compiled version]
    end

    H[Serve files]
    Z[Browser]

    D --> F
    G --vue-cli--> H
    D --ktor--> H
    H --> Z
    Z<-.Async queries.->F

```